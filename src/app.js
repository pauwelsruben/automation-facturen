import React from 'react';
import apiManager from './api/apiManager';
import {getInvoicesByNumber} from "./api/pm.api";
import {getInvoicesByDate} from "./api/pm.api";
import {getJournals} from "./api/pm.api";
import {getUserTemplates} from "./api/pm.api";
import {generatePDFFromInvoice} from "./api/pm.api";
import ShellExec from 'shelljs';
import SaveFile from 'save-file';
export default class App extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            status: '',
            status2: '',

            journalSelectValue: '',
            journalOpts: [],

            templateSelectValue: '',
            templateOpts: [],

            printerSelectValue: '',
            printerList: [],

            resultList: [],
            itemsToProcess: '',
            oldArray: '',

            invoicesList: [],
            invoiceNumbers: [],

            listToPrint: [],
            searchType: '',
        };

        this.handleChange           = this.handleChange.bind(this);
        this.getInvoiceByNumber     = this.getInvoiceByNumber.bind(this);
        this.downloadInvoices       = this.downloadInvoices.bind(this);
        this.printInvoices          = this.printInvoices.bind(this);
    }

    handleChange({target}) {
        //console.log(target.name, target.value);
        this.setState({
            [target.name]: target.value
        });
    }

    execute(command, callback) {
        const exec = require('child_process').exec;
        exec(command, (error, stdout, stderr) => {
            callback(stdout);
        });
    };

    componentDidMount(){

        if(localStorage.getItem('af_configvalid') && localStorage.getItem('af_configvalid') === '1'){
            this.fillSelectBoxes();
            this.resetLocalStorage();
            let fs = require('fs');
            let dir = './invoices';
            if(!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }

            this.setState({
                status: `Ready`
            });

        }else{
            this.setState({
                status: `Not ready`
            });
        }
    }

    resetLocalStorage() {
        localStorage.setItem('af_toBeMergedAuto', JSON.stringify([]));
        localStorage.setItem('af_toPrint', JSON.stringify([]));
        localStorage.setItem('af_invoiceNumbers', JSON.stringify([]));
    }

    fillSelectBoxes() {
        this.getJournals();
        this.getUserTemplates();
    }

    getJournals() {
        getJournals().then(response => {
            const journals      = response.journals;
            let arrJournalopts  = [];

            arrJournalopts.push(<option key={'default'} value={'default'}>Please select a journal</option>);

            journals.forEach(item => {
                arrJournalopts.push(<option key={item.Id} value={item.Id}>{item.Name}</option>)
            });

            this.setState({
                journalOpts: arrJournalopts
            });

        });
    }

    getUserTemplates() {
        getUserTemplates().then(response => {
            const templates     = response.usertemplates;
            //console.log(templates);
            let arrTemplates     = [];

            arrTemplates.push(<option key={'default'} value={'default'}>Please select a user template</option>);

            let oldValue = '';
            templates.forEach(item => {
                if(item.BaseName !== oldValue){
                    arrTemplates.push(<option key={item.BaseName} value={item.BaseName}>{item.BaseName}</option>)
                }
                oldValue = item.BaseName;
            });

            this.setState({
                templateOpts: arrTemplates
            });
        })
    }

    disableAllButtons() {
        let buttons = document.querySelectorAll('.btn');
        buttons.forEach(item => {
            item.setAttribute('disabled','disabled');
        });
    }

    disablePrintButtons() {
        let buttons = document.querySelectorAll('.btn:not(#searchBtn)');
        buttons.forEach(item => {
            item.setAttribute('disabled','disabled');
        });
    }

    enableAllButtons() {
        let buttons = document.querySelectorAll('.btn');
        buttons.forEach(item => {
            item.removeAttribute('disabled');
        });
    }

    downloadInvoices(type) {
        localStorage.setItem('af_toPrint',JSON.stringify([]));
        localStorage.setItem('af_invoiceNumbers',JSON.stringify([]));
        let invoices = this.state.invoicesList; //array met alle info over invoices

        if(invoices.length > 0){
            this.setState({
                itemsToProcess: invoices.length
            });

            /*const {dialog} = require('electron').remote;
            const pathDialog = dialog.showOpenDialog({
                title: 'Choose a folder',
                properties: ['openDirectory','createDirectory']
            });*/

            this.disableAllButtons();
            this.setState({
                status2: <span className={'flex flex-align'}><div className="circle-loader"><div className="checkmark draw"></div></div> Generating PDF (0/{invoices.length})</span>
            });
            if(type === 'print'){
                this.generateNewPDFPrint();
            }else if(type === 'download'){
                this.generateNewPDFDownload();
            }

        }else{
            this.setState({
                status2: <span>No invoices to download</span>
            });
        }
    }

    generateNewPDFFunction() {
        return new Promise(resolve => {
            let invoices            = this.state.invoicesList; //array met alle invoices

            document.querySelector('td[data-id="'+invoices[0].Id+'"]').innerHTML = 'Processing..';

            let toPrint             = JSON.parse(localStorage.getItem('af_toPrint')); // []
            let invoiceNumbers      = JSON.parse(localStorage.getItem('af_invoiceNumbers'));

            let str                 = localStorage.getItem('af_conn_hostname');
            let regex               = /https:\/\/(.*).planmanager.insypro.com\/api\/v3\//;
            let found               = str.match(regex);

            let fs = require('fs');
            let dir = './invoices/'+found[1];
            if(!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }

            // generate pdf from first invoice in list
            generatePDFFromInvoice(invoices[0].Id, this.state.templateSelectValue).then(response => {
                fs.writeFile('./invoices/' + found[1] + '/invoice_'+invoices[0].Number+'.pdf', response, 'base64', function(err){
                    if(err){
                        console.log(err);
                        this.setState({
                            status2: <span>Server failed to respond</span>
                        });
                    }else{
                        document.querySelector('td[data-id="'+invoices[0].Id+'"]').innerHTML = 'Done';
                        toPrint.push(invoices[0].Id);
                        localStorage.setItem('af_toPrint', JSON.stringify(toPrint)); //add generated file id to localstorage
                        invoiceNumbers.push(invoices[0].Number);
                        localStorage.setItem('af_invoiceNumbers', JSON.stringify(invoiceNumbers));
                        resolve(true);
                    }
                });
            });
        });
    }

    async generateNewPDFDownload() {
        let result = await this.generateNewPDFFunction();
        if(result){
            let invoices = this.state.invoicesList;
            this.setState({
                status2: <span className={'flex flex-align'}><div className="circle-loader"><div className="checkmark draw"></div></div> Generating PDF ({(this.state.itemsToProcess - invoices.length)+1}/{this.state.itemsToProcess})</span>
            });
            invoices.shift();
            this.setState({
                invoicesList: invoices
            });
            if(invoices.length > 0){
                this.generateNewPDFDownload();
            }else{
                this.enableAllButtons();
                this.setState({
                    status2: <span className={'flex flex-align'}><div className="circle-loader load-complete"><div className="checkmark draw"></div></div> Done generating</span>
                });
                document.querySelector('.checkmark').style.display = 'inline-block';
                //const {shell} = require('electron');
                //shell.openItem('./invoices');
            }
        }
    }

    async generateNewPDFPrint() {
        let result = await this.generateNewPDFFunction();
        if(result){
            let invoices = this.state.invoicesList;
            this.setState({
                status2: <span className={'flex flex-align'}><div className="circle-loader"><div className="checkmark draw"></div></div> Generating PDF ({(this.state.itemsToProcess - invoices.length)+1}/{this.state.itemsToProcess})</span>
            });
            invoices.shift(); //remove first element from invoices array
            this.setState({
                invoicesList: invoices
            });
            //check if there are invoices left to download
            if(invoices.length > 0){
                this.generateNewPDFPrint();
            }else{
                this.enableAllButtons();
                this.setState({
                    status2: <span className={'flex flex-align'}><div className="circle-loader load-complete"><div className="checkmark draw"></div></div> Done generating, next merging</span>
                });
                document.querySelector('.checkmark').style.display = 'inline-block';
                this.printInvoices(); //if all downloaded, print invoices
            }
        }
    }

    getInvoiceByNumber() {
        this.setState({
            status: `Gathering results..`,
            status2: ''
        });

        const numberStart       = this.state.numberStart;
        const numberEnd         = this.state.numberEnd;
        const dateStart         = this.state.dateStart;
        const dateEnd           = this.state.dateEnd;
        const journalSelected   = this.state.journalSelectValue;
        const templateSelected  = this.state.templateSelectValue;
        const searchType        = this.state.searchType;
        let orderType           = this.state.orderType;
        orderType               = orderType.toUpperCase();
        let resultArr           = [];

        if(journalSelected === 'default' || journalSelected === ''){
            this.setState({
                status: `Please select a journal`
            });
        }else if(searchType === ''){
            this.setState({
                status: `Please select a search type`
            });
        }else if(templateSelected === 'default' || templateSelected === ''){
            this.setState({
                status: `Please select a user template`
            });
        }else{
            this.enableAllButtons();
            if(searchType === 'date'){
                getInvoicesByDate(journalSelected, dateStart, dateEnd, orderType).then(response => {
                    if(response.invoices.length < 3000) {
                        const invoices  = response.invoices;
                        let invoiceNumbers = [];
                        invoices.forEach(item => {
                            invoiceNumbers.push(item.Number);
                            resultArr.push(<tr key={item.Id}><td>{item.Number}</td><td className={'overflow'}>{item.ClientName}</td><td>{item.Date}</td><td data-id={item.Id}>&nbsp;</td></tr>)
                        });
                        this.setState({
                            invoicesList: invoices,
                            invoiceNumbers: invoiceNumbers,
                            resultList: resultArr,
                            status: `${response.invoices.length} result(s) found`,
                            status2: ''
                        });
                    }else{
                        this.setState({
                            status: `Error`,
                            status2: 'Selected period too large'
                        });
                        this.disablePrintButtons();
                    }
                }, (reason) => {
                    this.disablePrintButtons();
                });
            }else if(searchType === 'number'){
                getInvoicesByNumber(journalSelected, numberStart, numberEnd, orderType).then(response => {
                    if(response.invoices.length < 3000) {
                        const invoices  = response.invoices;
                        let invoiceNumbers = [];
                        invoices.forEach(item => {
                            invoiceNumbers.push(item.Number);
                            resultArr.push(<tr key={item.Id}><td>{item.Number}</td><td className={'overflow'}>{item.ClientName}</td><td>{item.Date}</td><td data-id={item.Id}>&nbsp;</td></tr>)
                        });
                        this.setState({
                            invoicesList: invoices,
                            invoiceNumbers: invoiceNumbers,
                            resultList: resultArr,
                            status: `${response.invoices.length} result(s) found`,
                            status2: ''
                        });
                    } else{
                        this.setState({
                            status: `Error`,
                            status2: 'Selected period too large'
                        });
                        this.disablePrintButtons();
                    }
                }, (reason) => {
                    this.disablePrintButtons();
                });
            }

        }
    }

    printInvoiceFunction(){
        return new Promise(resolve => {

            let amountToMerge = 100;

            let all = JSON.parse(localStorage.getItem('af_invoiceNumbers'));
            let arr = JSON.parse(localStorage.getItem('af_invoiceNumbers')).slice(0,amountToMerge);
            const PDFMerge = require('pdf-merge');

            let str                 = localStorage.getItem('af_conn_hostname');
            let regex               = /https:\/\/(.*).planmanager.insypro.com\/api\/v3\//;
            let found               = str.match(regex);

            const arrFiles = arr.splice(0,arr.length).map(i => {
               return './invoices/'+ found[1] +'/invoice_'+i+'.pdf';
            });

            let orderType           = this.state.orderType;
            orderType               = orderType.toUpperCase();

            let outputName = '';
            if(orderType === 'ASC'){
                outputName = 'invoices_'+Math.min.apply(null, this.state.invoiceNumbers)+'-'+Math.max.apply(null, this.state.invoiceNumbers);
            }else{
                outputName = 'invoices_'+Math.max.apply(null, this.state.invoiceNumbers)+'-'+Math.min.apply(null, this.state.invoiceNumbers);
            }

            let fs = require('fs');
            let dir = './invoices/'+found[1]+'/'+outputName;
            if(!fs.existsSync(dir)){
                fs.mkdirSync(dir);
            }

            let easyPdfMerge    = require('easy-pdf-merge');

            console.log('merging: ' + arrFiles.length);

            let toBeMergedAuto = JSON.parse(localStorage.getItem('af_toBeMergedAuto')) ? JSON.parse(localStorage.getItem('af_toBeMergedAuto')) : [];
            
            let d = new Date();
            let t = d.getTime();
            easyPdfMerge(arrFiles, dir + '/' + outputName + '_' + t + '.pdf', function(err){
                if(err) return console.log(err);

                localStorage.setItem('af_invoiceNumbers', JSON.stringify(all.slice(amountToMerge, all.length)));

                toBeMergedAuto.push(dir + '/' + outputName + '_' + t + '.pdf');
                localStorage.setItem('af_toBeMergedAuto', JSON.stringify(toBeMergedAuto));

                let del = require('delete');
                del.promise(arrFiles, {force:true})
                    .then(function(){
                        if (all.slice(amountToMerge, all.length).length > 0) {
                            resolve(false);
                        } else {
                            console.log('doing final merge');
                            easyPdfMerge(JSON.parse(localStorage.getItem('af_toBeMergedAuto')), dir + '/' + outputName + '_final.pdf', function(err){
                                if(err) return console.log(err);
                
                                let del = require('delete');
                                del.promise(JSON.parse(localStorage.getItem('af_toBeMergedAuto')), {force:true})
                                    .then(function(){
                                        localStorage.setItem('af_toBeMergedAuto', JSON.stringify([]));
                                        resolve(true);
                                    });
                            });
                        }
                    });
            });

        });
    }

    async printInvoices() {
        let result = await this.printInvoiceFunction();
        if(result){
            this.enableAllButtons();
            this.setState({
                status2: <span className={'flex flex-align'}><div className="circle-loader load-complete"><div className="checkmark draw"></div></div> Invoices merged and ready to print</span>
            });
            document.querySelector('.checkmark').style.display = 'inline-block';
            //const {shell} = require('electron');
            //shell.openItem('./invoices');
        }else {
            this.printInvoices();
        }
    }

    searchType(event) {
        const value = event.target.value;
        this.setState({
            searchType: value
        });
        let dateEl      = document.querySelector('.searchDate');
        let numberEl    = document.querySelector('.searchNumber');
        if (value === 'date'){
            if(!dateEl.classList.contains('show')){
                dateEl.classList.add('show');
            }
            numberEl.classList.remove('show');
        }else if(value === 'number'){
            if(!numberEl.classList.contains('show')){
                numberEl.classList.add('show');
            }
            dateEl.classList.remove('show');
        }
    }

    render() {
        return (<div className={'container flex-between'}>

                <div className="searchForm">
                    <div className={'form-group flex flex-direction'}>
                        <select name={'journalSelectValue'} onChange={this.handleChange}>
                            {this.state.journalOpts}
                        </select>
                    </div>
                    <div className={'form-group flex flex-align radioList'}>
                        <div onChange={this.searchType.bind(this)}>
                            <input name="searchType" type="radio" value="number" id="number" /> <label className={'firstRadio'} htmlFor={'number'}>By number</label>
                            <input name="searchType" type="radio" value="date" id="date" /> <label htmlFor={'date'}>By date</label>
                        </div>
                    </div>
                    <div className={'form-group flex flex-between flex-direction searchNumber'}>
                        <div className={'flex flex-between'}>
                            <input type="text" name={'numberStart'} onChange={this.handleChange} placeholder={'start'}/>
                            <input type="text" name={'numberEnd'} onChange={this.handleChange} placeholder={'end'}/>
                        </div>
                    </div>
                    <div className={'form-group flex flex-between flex-direction searchDate'}>
                        <div>
                            <input type="date" name={'dateStart'} onChange={this.handleChange} placeholder={'start date'}/>
                            <input type="date" name={'dateEnd'} onChange={this.handleChange} placeholder={'end date'}/>
                        </div>
                    </div>
                    <div className={'form-group flex flex-direction'}>
                        <select name={'orderType'} onChange={this.handleChange}>
                            <option value="default" key={'default'}>Select ordering</option>
                            <option value="asc" key={'asc'}>Ascending</option>
                            <option value="desc" key={'desc'}>Descending</option>
                        </select>
                    </div>
                    <div className={'form-group flex flex-direction'}>
                        <select name={'templateSelectValue'} onChange={this.handleChange}>
                            {this.state.templateOpts}
                        </select>
                    </div>
                    <div className={'form-group flex'}>
                        <button id={'searchBtn'} className={'btn'} onClick={this.getInvoiceByNumber}>Search</button>
                    </div>
                    <div className={'form-group'}>
                        {this.state.status}
                    </div>
                    <div className={'form-group'}>
                        {this.state.status2}
                    </div>
                </div>
            <div className={'right flex flex-direction'}>
                <div className={'resultContainer'}>
                    <table className={'resultList'}>
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Name</th>
                                <th>Date</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                        {this.state.resultList}
                        </tbody>
                    </table>
                </div>
                <div className={'flex flex-between'}>
                    <div className={'flex btn-hor-group'}>
                        <div className={'form-group'}>
                            <button title={"Download all invoices and merge to 1 pdf for print"} onClick={() => this.downloadInvoices('print')} className={'btn'}><i className="fas fa-print"></i> Prepare for print</button>
                        </div>
                    </div>
                    <div className={'flex'}>
                        <div className={'form-group'}>
                            <button title={"Download all invoices without merging"} onClick={() => this.downloadInvoices('download')} className={'btn'}><i className="fas fa-download"></i> Download</button>
                        </div>
                    </div>
                </div>
            </div>

        </div>);
    }

}