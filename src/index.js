import {app, BrowserWindow, ipcMain, Menu} from 'electron';
import {version} from '../package.json';

/*require('update-electron-app')({
    repo: ''
});*/

// Handle creating/removing shortcuts on Windows when installing/uninstalling.
if (require('electron-squirrel-startup')) { // eslint-disable-line global-require
  app.quit();
}

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow;
let configWindow;

ipcMain.on('async', (event, arg) => {
    if (arg === 'reload') {
        configWindow.close();
        mainWindow.reload();
    }
});

const createWindow = () => {

    const menu = Menu.buildFromTemplate([
        {
            label: 'Menu',
            submenu: [
                {
                    label: 'Edit config', click: () => {
                        configWindow = new BrowserWindow({
                            width: 1000,
                            height: 600,
                            show: true,
                            center: true,
                            resizable: false
                        });
                        configWindow.loadURL(`file://${__dirname}/config.html`);
                        //configWindow.webContents.openDevTools();
                    }
                },
                {
                    label: 'Reset config', click: () => {
                        dialog.showMessageBox({
                            type: 'question',
                            title: 'Reset config',
                            message: 'Are you sure you want to reset?',
                            buttons: ['Cancel', 'OK']
                        }, (response) => {
                            if (response === 0) {
                                /** cancel **/
                            } else {
                                /** ok **/
                                const ses = mainWindow.webContents.session;
                                ses.clearStorageData({
                                    options: ['localstorage']
                                }, () => {
                                    app.relaunch();
                                    setTimeout(() => {
                                        app.exit();
                                    }, 50);
                                });
                            }
                        });
                    }
                },
                {type: 'separator'},
                {
                    label: 'Exit', click: () => {
                        app.quit();
                    }
                }
            ]
        },
        {
            label: 'View',
            submenu: [
                {role: 'reload'},
                {role: 'forcereload'},
                {role: 'toggledevtools'},
                {type: 'separator'},
                {role: 'resetzoom'},
                {role: 'zoomin'},
                {role: 'zoomout'},
                {type: 'separator'}
            ]
        },
        {
            label: 'About',
            submenu: [
                {
                    label: 'InSyPro website', click: () => {
                        require('electron').shell.openExternal('https://insypro.com/')
                    }
                },
                {
                    label: version,
                    enabled: false
                }
            ]
        }
    ]);
    Menu.setApplicationMenu(menu);

  // Create the browser window.
  mainWindow = new BrowserWindow({
    width: 1024,
    height: 600,
  });

  // and load the index.html of the app.
  mainWindow.loadURL(`file://${__dirname}/index.html`);

  // Open the DevTools.
  //mainWindow.webContents.openDevTools();

  // Emitted when the window is closed.
  mainWindow.on('closed', () => {
    // Dereference the window object, usually you would store windows
    // in an array if your app supports multi windows, this is the time
    // when you should delete the corresponding element.
    mainWindow = null;
  });
};

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Quit when all windows are closed.
app.on('window-all-closed', () => {
  // On OS X it is common for applications and their menu bar
  // to stay active until the user quits explicitly with Cmd + Q
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  // On OS X it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (mainWindow === null) {
    createWindow();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and import them here.
