import React from 'react';
import fs from 'fs';

export default class Config extends React.Component {

    constructor(props) {
        super(props);
        this.state = {
            messages: [],
            dirscanned: false,
            passwordSet: !!localStorage.getItem('af_conn_password'),
            connHostnameInput: '' || localStorage.getItem('af_conn_hostname'),
            connUsernameInput: '' || localStorage.getItem('af_conn_username'),
            connPasswordInput: ''
        };
        this.parseXmlOptions = {
            trim: true,
            explicitArray: false,
            normalizeTags: true,
            ignoreAttrs: true

        };
        this.setConfig = this.setConfig.bind(this);
        this.handleChange = this.handleChange.bind(this);
    }

    handleChange({target}) {
        this.setState({
            [target.name]: target.value
        });
    }

    setConfig() {
        const connHostname          = this.state.connHostnameInput;
        const connUsername          = this.state.connUsernameInput;
        const connPassword          = this.state.connPasswordInput;
        const isPwSet               = !!localStorage.getItem('af_conn_password');
        const CryptoJS              = require('crypto-js');
        const secretPhrase          = '1NsYpR00RpYsN1';
        const required              = [connHostname, connUsername];
        for(let value of required){
            if(value === '' || value == null || (!isPwSet && (connPassword === '' || connPassword == null))){
                localStorage.setItem('af_configvalid', '0');
                break;
            }else{
                //initial check if password is in storage, if not check if password field has input
                if(!isPwSet){
                    if(connPassword === '') {
                        localStorage.setItem('af_configvalid', '0');
                        break;
                    }else{
                        localStorage.setItem('af_configvalid', '1');
                        const encrypted = CryptoJS.AES.encrypt(connPassword, secretPhrase);
                        localStorage.setItem('af_conn_password', encrypted);
                    }
                }else{
                    if(connPassword !== ''){
                        const encrypted = CryptoJS.AES.encrypt(connPassword, secretPhrase);
                        localStorage.setItem('af_conn_password', encrypted);
                    }
                    localStorage.setItem('af_configvalid', '1');
                }
            }
        }

        const {dialog} = require('electron').remote;
        if(localStorage.getItem('af_configvalid') === '1'){
            localStorage.setItem('af_conn_hostname', connHostname);
            localStorage.setItem('af_conn_username', connUsername);

            dialog.showMessageBox({
                title: 'Configuration saved',
                message: 'Succesfully saved configuration!',
                detail: 'The app will now reload',
                type: 'info',
                buttons: ['OK']
            });
            let {ipcRenderer} = require('electron');
            ipcRenderer.send('async', 'reload');
        }else{
            dialog.showMessageBox({
                title: 'Error',
                message: 'Please fill in all fields.',
                type: 'error',
                buttons: ['OK']
            });
        }
    }

    componentDidMount() {
    }

    render() {
        const passwordSet = this.state.passwordSet;
        let icon,pwinfo;
        if(passwordSet){
            icon = <i className="fas fa-lock"></i>;
            pwinfo = <span>(Password is set but not shown<br/>for security reasons)</span>;
        }else{
            icon = <i className="fas fa-unlock-alt"></i>;
            pwinfo = <span></span>;
        }
        return (<div>
            <h2><i className={'fas fa-cog rotating'}></i> Configuration</h2>
            <div className="flex">
                <div className="flex dbConfig">
                    <div className={'form-group'}>
                        <i className="fas fa-server"></i>
                        <label>Hostname:</label>
                        <input name="connHostnameInput" onChange={this.handleChange}
                               value={this.state.connHostnameInput} type="text"/>
                    </div>
                    <div className={'form-group'}>
                        <i className="fas fa-user-astronaut"></i>
                        <label>Username:</label>
                        <input name="connUsernameInput" onChange={this.handleChange}
                               value={this.state.connUsernameInput} type="text"/>
                    </div>
                    <div className={'form-group'}>
                        {icon}
                        <label>Password:</label>
                        <input name="connPasswordInput" onChange={this.handleChange}
                               value={this.state.connPasswordInput} type="password"/>
                        {pwinfo}
                    </div>
                </div>
            </div>
            <div className="flex">
                <div>
                    <button onClick={this.setConfig}>Save</button>
                </div>
            </div>
        </div>);
    }
}
