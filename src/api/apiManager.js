import axios from 'axios';

const localHost     = localStorage.getItem('af_conn_hostname') || 'host';
const localUser     = localStorage.getItem('af_conn_username') || 'user';
const localPw       = localStorage.getItem('af_conn_password') || 'pass';
const CryptoJS      = require('crypto-js');
const secretPhrase  = '1NsYpR00RpYsN1';
const bytes         = CryptoJS.AES.decrypt(localPw.toString(), secretPhrase);
const plaintextPw   = bytes.toString(CryptoJS.enc.Utf8);

const USERNAME      = localUser;
const PASSWORD      = plaintextPw;

const apiManager = axios.create({
    baseURL: localHost,
    timeout: 10000,
});

apiManager.interceptors.request.use(
    config => {
        if (USERNAME && PASSWORD) {
            config.headers.Authorization = `Basic ${btoa(`${USERNAME}:${PASSWORD}`)}`;
        }
        return config;
    },
    error => Promise.reject(error),
);

apiManager.interceptors.response.use(
    response => response,
    error => {
        console.log('Request failed:', error.config);
        if(error.response){
            console.error('Status:', error.response.status);
            console.error('Data:', error.response.data);
            console.error('Headers:', error.response.headers);
        }else{
            console.error('Error message:', error.message);
        }
        return Promise.reject(error.response || error.message);
    }
);

export default apiManager;