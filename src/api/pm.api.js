import apiManager from './apiManager';

export const getInvoicesByNumber = (journalId, numberStart, numberEnd, orderType) =>
    apiManager({
        method: 'get',
        url: `invoices/{"filter":{"JournalId":${journalId},"Type":"sale invoice","Number":{"greater_or_equal":"${numberStart}","less_or_equal":"${numberEnd}"}},"Orderby":"Number+${orderType}","limit":"10000"}`
    }).then(resp => resp.data);

export const getInvoicesByDate = (journalId, dateStart, dateEnd, orderType) =>
    apiManager({
        method: 'get',
        url: `invoices/{"filter":{"JournalId":${journalId},"Type":"sale invoice","Number":{"is_null":false},"Date":{"greater_or_equal":"${dateStart}","less_or_equal":"${dateEnd}"}},"Orderby":"Number+${orderType}","limit":"10000"}`
    }).then(resp => resp.data);

export const getJournals = () =>
    apiManager({
        method: 'get',
        url: `journals/{"filter":{"Type":"sale invoice"}}`
    }).then(resp => resp.data);

export const getUserTemplates = () =>
    apiManager({
        method: 'get',
        url: `usertemplates/{"filter":{"Type":"invoice"}}`
    }).then(resp => resp.data);

export const generatePDFFromInvoice = (id, template) =>
    apiManager({
        method: 'get',
        url: `invoices/{"filter":{"Id":${id}},"generate_pdf":"${template}"}`,
        headers: {
            'Content-Transfer-Encoding': 'base64'
        }
    }).then(resp => resp.data);